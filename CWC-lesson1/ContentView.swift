//
//  ContentView.swift
//  tutorialMJ
//
//  Created by Maciek Janik on 28/12/2023.
//

import SwiftUI
import SwiftData

struct ContentView: View {

    var body: some View {
        ZStack{
                Color(.black)
                    .ignoresSafeArea()
                    .background()
            
            
            
            VStack {
                Image("tt")
                    .resizable()
                    .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                    .cornerRadius(100)
                    .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fit/*@END_MENU_TOKEN@*/)
                
                Text("Moje TT")
                    .font(/*@START_MENU_TOKEN@*/.largeTitle/*@END_MENU_TOKEN@*/)
                            .bold()
                            .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
            }
        }
        
        
        

        
        
    }
}

#Preview {
    ContentView();
}
