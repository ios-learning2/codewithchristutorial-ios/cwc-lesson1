//
//  CWC_lesson1App.swift
//  CWC-lesson1
//
//  Created by Maciek Janik on 28/12/2023.
//

import SwiftUI

@main
struct CWC_lesson1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
